---
layout: default
title: Resources
---

These are articles, documents, and web pages that you might find of use or interest throughout the term. Some will be explicitly referenced; others are here for your enjoyment.

### Local Resources
<table id="local" class="table table-striped table-hover">
  <tbody>
{% for res in site.data.resources.resources %}
<tr>
  <td width="10%"> {% include fa name=res.icon %} </td>
  <td><a href="{{res.link}}">{{res.title}}</a></td>
</tr>
{% endfor %}
  </tbody>
</table>

{% assign articleCount = site.data.resources.articles | size %}
{% if articleCount > 0 %}
### Articles

<table id="local" class="table table-striped table-hover">
  <tbody>
{% for res in site.data.resources.articles %}
<tr>
  <td width="10%"> {% include fa name=res.icon %} </td>
  <td><a href="{{res.link}}">{{res.title}}</a></td>
</tr>
{% endfor %}
  </tbody>
</table>

{% endif %}